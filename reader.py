import asyncio
import json
import signal
import sys
from asyncio import CancelledError
from concurrent.futures import ProcessPoolExecutor

import websockets
from smartcard.CardRequest import CardRequest
from smartcard.CardType import AnyCardType

CONNECTIONS = set()


async def handler(websocket):
    CONNECTIONS.add(websocket)
    try:
        await websocket.wait_closed()
    finally:
        CONNECTIONS.remove(websocket)


def get_uuid():
    card_type = AnyCardType()
    try:
        request = CardRequest(timeout=None, cardType=card_type, newcardonly=True)
        service = request.waitforcard()
        service.connection.connect()
        response, sw1, sw2 = service.connection.transmit([0xFF, 0xCA, 0x00, 0x00, 0x00])
        return response
    except Exception:
        pass
    return None


def handle(*args):
    sys.exit(0)


def init():
    signal.signal(signal.SIGINT, handle)


async def poll_loop():
    while True:
        p = ProcessPoolExecutor(1, initializer=init)
        try:
            response = await asyncio.get_event_loop().run_in_executor(p, get_uuid)
        except CancelledError:
            p.shutdown(wait=False, cancel_futures=True)
            return
        if response is not None:
            websockets.broadcast(CONNECTIONS, json.dumps({'uuid': response}))


async def server():
    t = asyncio.create_task(poll_loop())
    async with websockets.serve(handler, "localhost", 8888):
        await t


if __name__ == '__main__':
    asyncio.run(server())
